'use strict';
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
const nodemailer = require('nodemailer');

let transporter = nodemailer.createTransport({
    host: 'smtp.zoho.com',
	port: 465 ,
	secure: true, 
    auth: {
        user: 'support@stonevire.com',
        pass: 'Stone@0810@support#95'
    },
	tls: {
        rejectUnauthorized: false
    }
});

 let mailOptions = {
    from: '"Stone Support" <support@stonevire.com>', // sender address
    //to: 'saksham@stonevire.com, khrn.saksham001@gmail.com', // list of receivers
	to: 'saksham@stonevire.com',
    subject: 'Hello ✔', // Subject line
    text: 'Hello world ? Hellllooooo', // plain text body
    //html: '<b>Hello world ?</b>' // html body
};


// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    //res.json({ message: 'hooray! welcome to our api!' });   
	//res.sendFile(__dirname + '/DI.png');
	
	//firstFile.em.emit('FirstEvent', res , req);
	
	transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
        return console.log(error);
    }
    console.log('Message %s sent: %s', info.messageId, info.response);
	});
	
	res.json({ message: "Done" });
});

router.get('/hello', function(req, res) {
	jwt.verify(req.query.token, 'secret', {audience: req.query.name }, function(err, decoded) {
  if (err) {
   res.json(err); 
  }else{
	  res.json({ name: decoded.name , pin : decoded.hello});
  }
});
   //jwt.verify(req.query.token, 'secret', function(err, decoded) {
  //if (err) {
   //res.json(err); 
  //}else{
	//  res.json({ name: decoded.data , pin : decoded.hello});
 // }
});



router.route('/getting')

	.post(function(req,res){
		
		//res.json({ message: req.body.name});
		res.sendFile(__dirname + '/DI.png');
	});

// more routes for our API will happen here

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);