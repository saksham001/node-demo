var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');
var multer = require('multer');
var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'he/')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
});

var upload = multer({storage:storage});


//app.use(bodyParser.urlencoded({ extended: true }));
//app.use(bodyParser.json());

var port = process.env.PORT || 8083;        
var router = express.Router();              

var url = bodyParser.urlencoded({ extended: true })

app.post('/', url , function(req, res) {
    res.json({ message: req.body.name});   

});

app.post('/upload', upload.single('foo'), function(req, res) {
  if (req.file) {
    console.dir(req.file);
    return res.end('Thank you for the file');
  }
  res.end('Missing file');
});

router.route('/getting')

	.post(url,function(req,res){
		res.json({ message: req.body.name});   
		
	});
	
router.route('/upload1')

	.post(upload.single('foo'),function(req,res){
		if (req.file) {
    console.dir(req.file);
    return res.end('Thank you for the file');
  }
  res.end('Missing file');
		
	});

app.use('/api', router);
app.listen(port);
console.log('Magic happens on port ' + port);