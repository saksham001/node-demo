var express    = require('express');
var app        = express();
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8001;
var router = express.Router();

router.get('/', function(req, res) {
    
    var a;
    switch(req.query.name)
    {
            case "hello"         : a="h1ello";break;
            case "bello"         : a="b1ello";break;
            case "jello"         : a="j1ello";break;
    }
    
    res.json({name:a});
            
});

app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);